## Pixel Calibration

Code for the IBL/PIXEL calibration. Run:
```
sh Calib.sh
```
changing the layer being calibrated (`WhichPart`), and number of injected charges (`ncharge`, `chargeArr` and `chargeErrArr`) within `PixelCalib.C`.

The new IBL calibration can be run through:
```
. Make.sh
./CalibrateIBL.exe
```
